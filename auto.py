from subprocess import Popen, PIPE
import re
import os
import json


def minimum(a, b):

    if a <= b:
        return a
    else:
        return b


def create_plot_files(i):

    with open(i+'/basic_config.gnuplot', 'r') as src_file, open(i+'/gnuplot/config.gnuplot', 'w') as dest_file:
        # Read the contents of the source file
        file_contents = src_file.read()

        # Write the contents to the destination file
        dest_file.write(file_contents)

        # Append some additional text to the destination file
        dest_file.write("set title 'Encaminamiento" +i +"'\n")
        dest_file.write("set key left top\n")
        dest_file.write("set output './traficos_a.png'\n")
        dest_file.write("plot 'trafico_a.plot' with yerrorlines linecolor rgb '#001DBC' lw 2 title 'Tráfico A' \n")
        dest_file.write("pause 1\n")

        dest_file.write("set output './traficos_b.png'\n")
        dest_file.write("set key left top\n")
        dest_file.write("plot 'trafico_b.plot' with yerrorlines linecolor rgb '#B85450' lw 2 title 'Tráfico B' \n")
        dest_file.write("pause 1\n")

        dest_file.write("set output './traficos_c.png'\n")
        dest_file.write("set key left top\n")
        dest_file.write("plot 'trafico_c.plot' with yerrorlines linecolor rgb '#B09500' lw 2 title 'Tráfico C' \n")
        dest_file.write("pause 1\n")

        dest_file.write("set output './traficos_d.png'\n")
        dest_file.write("set key left top\n")
        dest_file.write("plot 'trafico_d.plot' with yerrorlines linecolor rgb '#3A5431' lw 2 title 'Tráfico D' \n")
        dest_file.write("pause 1\n")





def generate_plot(A, data,i):

    bloqueo = []

    bloqueo.append(list(data.items())[0])  # Corresponde con el promediador a
    bloqueo.append(list(data.items())[1])  # Corresponde con el promediador b
    bloqueo.append(list(data.items())[2])  # Corresponde con el promediador c
    bloqueo.append(list(data.items())[3])  # Corresponde con el promediador d


    for bloqueo_trafico in bloqueo:

        promediador = bloqueo_trafico[0]
        AC_min = A*(1-float(bloqueo_trafico[1]['Confidence interval 1'][0]))
        AC_max = A*(1-float(bloqueo_trafico[1]['Confidence interval 1'][1]))
        AC_med = A*(1-float(bloqueo_trafico[1]['Probabilidad de bloqueo']))

        with open(i+'/gnuplot/trafico_'+str(promediador)+'.plot', 'a+') as fw:
            fw.write(""+str(A)+" "+str(AC_med)+" " +
                     str(AC_min)+" "+str(AC_max)+"\n")










print("Introduce rango de valores de r (ej -> 0.1/5.0): ", end = "")
rango_r = input()
min_r = float (rango_r.split("/")[0])
max_r = float (rango_r.split("/")[1])

print("Introduce valor de m: ", end="")
m = input()

print("Introduce valor de S (tiempo de servicio): ", end="")
S = input()

print("Introduce valor de q (intervalo de confianza): ", end="")
q = input()

print("Introduce valor de t (tolerancia): ", end="")
t = input ()

print("Introduce valor de seed: ", end="")
seed = input()

print("Introduce lo/s encaminamiento/s (I,II,III,IV): ", end="")
encs = input ().upper ().split (',')

iteracion = 0
PBlockAnt = 0


### ESTO ES PARA PLOTTEAR



for i in encs:
    for file_name in os.listdir('./' + i + '/gnuplot' ):
      file_path = os.path.join('./' + i + '/gnuplot' , file_name)
      if os.path.isfile(file_path):
          os.remove(file_path)
    create_plot_files(i)  # Tantos plot files como casos tengamos

    r = min_r

    while r <= max_r + 0.1:

        #####################################
        ######### Calculamos lambda #########
        #####################################

        # Lambda_base es el de los tráficos sin división y lambda_medio el del tráfico dividido (d)

        lambda_base = r * 80 / (int(S) * 7)
        lambda_medios = r * 80 / ((int(S) * 2) * 7)

        print("Lambda base "+str(lambda_base))
        print("Lambda medios "+str(lambda_medios))

        ######################################
        #### Generamos fichero config.cfg ####
        ######################################

        with open(i + '/basic_config.cfg', 'r') as fr, open(i + '/config/config_'+str(format (r, '.1f'))+'.cfg', 'w') as fw:

            base_substitution = str(1/lambda_base)
            medios_substitution = str(1/lambda_medios)

            for line in fr:
                # Replace all instances of "SIMPLE" with the constant substitution string
                modified_line = line.replace("LAMBDA_BASE", base_substitution).replace(
                    "LAMBDA_MEDIOS", medios_substitution).replace(
                    "TIEMPO_SERVICIO", str(S))
                # Write the modified line to the output file
                fw.write(modified_line)

        #####################################
        ###### Ejecutamos el simulador ######
        #####################################

        if (i != 'IV'):

            process = Popen(["./SimRedMMkk", "-s", str(seed), "-q", str(q),
                            "-t", str(t), "-a", i + "/config/config_"+str(format (r, '.1f'))+".cfg"], stdout=PIPE)
            (output, err) = process.communicate()
            exit_code = process.wait()

            os.rename(i + "/config/config_"+str(format (r, '.1f'))+".cfg.out",
                      i + "/config_out/config_"+str(format (r, '.1f'))+".cfg.out")

        else:
            process = Popen(["./SimRedMMkk", "-s", str(seed), "-q", str(q),
                            "-t", str(t), "-n", "4", "-a", i + "/config/config_"+str(format (r, '.1f'))+".cfg"], stdout=PIPE)
            (output, err) = process.communicate ()
            exit_code = process.wait ()

            os.rename(i + "/config/config_"+str(format (r, '.1f'))+".cfg.4k.out",
                      i + "/config_out/config_"+str(format (r, '.1f'))+".cfg.out")

        
        

        ##################################################
        #### Guardamos los valores en el fichero plot ####
        ##################################################

        with open(i + '/config_out/config_'+str(format (r, '.1f'))+'.cfg.out', 'r') as f:
            lines = f.readlines()

        data = {}
        current_block = 0

        for line in lines:
            line = line.strip()
            if line.startswith ('0.'): break
            
            if line.startswith('Trafico/s promediado/s'):
                current_block = line.split(" ")[-1][0]
            if line.startswith('Probabilidad de bloqueo estimada:'):
                data[current_block] = {}
                data[current_block]['Probabilidad de bloqueo'] = float(
                    line.split(":")[1])
            elif line.startswith('Confidence interval 1:'):
                print(re.findall('\((.*?)\)', line))
                data[current_block]['Confidence interval 1'] = tuple(
                    [float(num) for num in re.findall('\((.*?)\)', line)[0].split(",")])
            elif line.startswith('Confidence interval 2:'):
                data[current_block]['Confidence interval 2'] = tuple(
                    [float(num) for num in re.findall('\((.*?)\)', line)[0].split(",")])

        # print(data)

        with open(i + '/config_out/data_'+str(format (r, '.1f'))+'.json', 'w') as f:
            json.dump(data, f, indent=4)

        A = float(lambda_base)*int(S)
        r += 0.1 

        generate_plot(A, data,i)

    iteracion += 1

    os.chdir(i+'/gnuplot')

    process = Popen(["gnuplot", "config.gnuplot"], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()
    os.chdir('../../')
