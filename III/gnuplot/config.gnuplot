set terminal png size 1920,1080
# Poner etiqueta en eje de abscisas:
set xlabel 'A'
# Ídem ordenadas
set ylabel 'Ac'
# Fijar posición de la leyenda de funciones
set key bottom right
set title 'EncaminamientoIII'
set key left top
set output './traficos_a.png'
plot 'trafico_a.plot' with yerrorlines linecolor rgb '#001DBC' lw 2 title 'Tráfico A' 
pause 1
set output './traficos_b.png'
set key left top
plot 'trafico_b.plot' with yerrorlines linecolor rgb '#B85450' lw 2 title 'Tráfico B' 
pause 1
set output './traficos_c.png'
set key left top
plot 'trafico_c.plot' with yerrorlines linecolor rgb '#B09500' lw 2 title 'Tráfico C' 
pause 1
set output './traficos_d.png'
set key left top
plot 'trafico_d.plot' with yerrorlines linecolor rgb '#3A5431' lw 2 title 'Tráfico D' 
pause 1
