set terminal png size 1920,1080
# Poner etiqueta en eje de abscisas:
set xlabel 'A'
# Ídem ordenadas
set ylabel 'Ac'
# Fijar posición de la leyenda de funciones
set key bottom right
set title 'Encaminamiento A'
set key left top
set output './gnuplot/traficos_a.png'
plot './gnuplot/plot_a.plot' using 1:2 with linespoints linecolor rgb '#001DBC' lt 1 lw 2 title 'Tráfico Teórico', '../gnuplot/trafico_a.plot' using 1:2 with linespoints linecolor rgb '#B89500' lt 1 lw 2 title 'Tráfico Simulado'
pause 1

set title 'Encaminamiento C'
set key left top
set output './gnuplot/traficos_c.png'
plot './gnuplot/plot_c.plot' using 1:2 with linespoints linecolor rgb '#001DBC' lt 1 lw 2 title 'Tráfico Teórico', '../gnuplot/trafico_b.plot' using 1:2 with linespoints linecolor rgb '#B89500' lt 1 lw 2 title 'Tráfico Simulado'
pause 1

set title 'Encaminamiento D'
set key left top
set output './gnuplot/traficos_d.png'
plot './gnuplot/plot_d.plot' using 1:2 with linespoints linecolor rgb '#001DBC' lt 1 lw 2 title 'Tráfico Teórico', '../gnuplot/trafico_c.plot' using 1:2 with linespoints linecolor rgb '#B89500' lt 1 lw 2 title 'Tráfico Simulado'
pause 1

set title 'Encaminamiento F'
set key left top
set output './gnuplot/traficos_f.png'
plot './gnuplot/plot_f.plot' using 1:2 with linespoints linecolor rgb '#001DBC' lt 1 lw 2 title 'Tráfico Teórico', '../gnuplot/trafico_d.plot' using 1:2 with linespoints linecolor rgb '#B89500' lt 1 lw 2 title 'Tráfico Simulado'
pause 1