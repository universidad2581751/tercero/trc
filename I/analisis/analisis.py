import subprocess
import csv


print("Introduce rango de valores de r (ej -> 0.1/5.0): ", end="")
rango_r = input()
r = float (rango_r.split ("/") [0])
max_r = float (rango_r.split ("/") [1])

print("Introduce valor de m: ", end="")
m = input()

datosA = []
datosC = []
datosD = []
datosF = []

plotA = []
plotC = []
plotD = []
plotF = []


i = 0

while r <= max_r + 0.1:

    A = (80 * r) / 7    

    A1 = A

    p = subprocess.run ('tclsh Erlang.tcl ' + str (m) + ' ' + str (A1), shell=True, check=True, capture_output=True,encoding='utf-8')
    Be1 = float(p.stdout.split()[0])

    A2 = A + A1 * (1 - Be1)
    p = subprocess.run('tclsh Erlang.tcl ' + str (m) + ' ' + str (A2), shell=True, check=True, capture_output=True, encoding='utf-8')
    Be2 = float (p.stdout.split()[0])

    A3 = A / 2
    p = subprocess.run('tclsh Erlang.tcl ' + str (m) + ' ' + str (A3), shell=True, check=True, capture_output=True, encoding='utf-8')
    Be3 = float (p.stdout.split()[0])

    A4 = A + A2 * (1 - Be2) + A3 * (1 - Be3)
    p = subprocess.run('tclsh Erlang.tcl ' + str (m) + ' ' + str (A4), shell=True, check=True, capture_output=True, encoding='utf-8')
    Be4 = float (p.stdout.split()[0])

    Bt_a = 1 - ((1 - Be1) * (1 - Be2) * (1 - Be4))
    Bt_c = 1 - ((1 - Be2) * (1 - Be4))
    Bt_d = 1 - ((1 - Be3) * (1 - Be4))
    Bt_f = 1 - ((1 - Be4))

    Ac_a = A * (1 - Bt_a)
    Ac_c = A * (1 - Bt_c)
    Ac_d = A * (1 - Bt_d)
    Ac_f = A * (1 - Bt_f)

    datosA.append ([format(A, '.5f'), format (A1, '.5f'), format (Be1, '.5e'), format (A2, '.5f'), format (Be2, '.5e'), format (A4, '.5f'), format (Be4, '.5e'), format (Ac_a, '.5f'), format (Bt_a, '.5e')])
    datosC.append ([format(A, '.5f'), format (A2, '.5f'), format (Be2, '.5e'), format (A4, '.5f'), format (Be4, '.5e'), format (Ac_a, '.5f'), format (Bt_a, '.5e')])
    datosD.append ([format(A, '.5f'), format (A3, '.5f'), format (Be3, '.5e'), format (A4, '.5f'), format (Be4, '.5e'), format (Ac_a, '.5f'), format (Bt_a, '.5e')])
    datosF.append ([format(A, '.5f'), format (A4, '.5f'), format (Be4, '.5e'), format (Ac_a, '.5f'), format (Bt_a, '.5e')])

    plotA.append ([str (A), str (Ac_a)])
    plotC.append ([str (A), str (Ac_c)])
    plotD.append ([str (A), str (Ac_d)])
    plotF.append ([str (A), str (Ac_f)])

    r += 0.1


with open('datos/datosA.csv', 'w', newline='') as file:
    writer = csv.writer(file, quoting=csv.QUOTE_NONE)
    writer.writerows(datosA)

with open('datos/datosC.csv', 'w', newline='') as file:
    writer = csv.writer(file, quoting=csv.QUOTE_NONE)
    writer.writerows(datosC)

with open('datos/datosD.csv', 'w', newline='') as file:
    writer = csv.writer(file, quoting=csv.QUOTE_NONE)
    writer.writerows(datosD)

with open('datos/datosF.csv', 'w', newline='') as file:
    writer = csv.writer(file, quoting=csv.QUOTE_NONE)
    writer.writerows(datosF)



with open('gnuplot/plot_a.plot', 'w') as file:
    for i in plotA:
        file.write (i [0] + " " + i [1] + "\n")

with open('gnuplot/plot_c.plot', 'w') as file:
    for i in plotC:
        file.write (i [0] + " " + i [1] + "\n")

with open('gnuplot/plot_d.plot', 'w') as file:
    for i in plotD:
        file.write (i [0] + " " + i [1] + "\n")

with open('gnuplot/plot_f.plot', 'w') as file:
    for i in plotF:
        file.write (i [0] + " " + i [1] + "\n")